<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230609102707 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE type_contact (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(150) NOT NULL, icon VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_profil (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, genre VARCHAR(150) NOT NULL, nom VARCHAR(150) NOT NULL, prenom VARCHAR(150) NOT NULL, naissance_at DATE DEFAULT NULL, avatar VARCHAR(255) DEFAULT NULL, compte INT DEFAULT NULL, biographie LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_8384A9AAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_profil_contact (id INT AUTO_INCREMENT NOT NULL, profil_user_id INT DEFAULT NULL, type_contact_id INT DEFAULT NULL, content VARCHAR(200) DEFAULT NULL, INDEX IDX_A2F3F630227A1CC4 (profil_user_id), INDEX IDX_A2F3F630FAA2F36F (type_contact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_profil ADD CONSTRAINT FK_8384A9AAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_profil_contact ADD CONSTRAINT FK_A2F3F630227A1CC4 FOREIGN KEY (profil_user_id) REFERENCES user_profil (id)');
        $this->addSql('ALTER TABLE user_profil_contact ADD CONSTRAINT FK_A2F3F630FAA2F36F FOREIGN KEY (type_contact_id) REFERENCES type_contact (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_profil DROP FOREIGN KEY FK_8384A9AAA76ED395');
        $this->addSql('ALTER TABLE user_profil_contact DROP FOREIGN KEY FK_A2F3F630227A1CC4');
        $this->addSql('ALTER TABLE user_profil_contact DROP FOREIGN KEY FK_A2F3F630FAA2F36F');
        $this->addSql('DROP TABLE type_contact');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_profil');
        $this->addSql('DROP TABLE user_profil_contact');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
