<?php

namespace App\Controller\Utilisateur;

use App\Entity\Utilisateur\UserProfil;
use App\Form\Utilisateur\ProfilUserType;
use App\Repository\Utilisateur\UserProfilRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('backoffice/utilisateur/profil')]
class UserProfilController extends AbstractController
{
    #[Route('/', name: 'app_utilisateur_profil_user_index', methods: ['GET'])]
    public function index(UserProfilRepository $profilUserRepository): Response
    {
        return $this->render('utilisateur/profil_user/index.html.twig', [
            'profil_users' => $profilUserRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_utilisateur_profil_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserProfilRepository $profilUserRepository): Response
    {
        $profilUser = new UserProfil();
        $form = $this->createForm(ProfilUserType::class, $profilUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $profilUserRepository->save($profilUser, true);

            return $this->redirectToRoute('app_utilisateur_profil_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/profil_user/new.html.twig', [
            'profil_user' => $profilUser,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_profil_user_show', methods: ['GET'])]
    public function show(UserProfil $profilUser): Response
    {
        return $this->render('utilisateur/profil_user/show.html.twig', [
            'profil_user' => $profilUser,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_utilisateur_profil_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserProfil $profilUser, UserProfilRepository $profilUserRepository): Response
    {
        $form = $this->createForm(ProfilUserType::class, $profilUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $profilUserRepository->save($profilUser, true);

            return $this->redirectToRoute('app_utilisateur_profil_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/profil_user/edit.html.twig', [
            'profil_user' => $profilUser,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_profil_user_delete', methods: ['POST'])]
    public function delete(Request $request, UserProfil $profilUser, UserProfilRepository $profilUserRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$profilUser->getId(), $request->request->get('_token'))) {
            $profilUserRepository->remove($profilUser, true);
        }

        return $this->redirectToRoute('app_utilisateur_profil_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
