<?php

namespace App\Controller\Utilisateur\Lib;

use App\Entity\Utilisateur\Lib\TypeContact;
use App\Form\Utilisateur\Lib\TypeContactType;
use App\Repository\Utilisateur\Lib\TypeContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/utilisateur/lib/type/contact')]
class TypeContactController extends AbstractController
{
    #[Route('/', name: 'app_utilisateur_lib_type_contact_index', methods: ['GET'])]
    public function index(TypeContactRepository $typeContactRepository): Response
    {
        return $this->render('utilisateur/lib/type_contact/index.html.twig', [
            'type_contacts' => $typeContactRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_utilisateur_lib_type_contact_new', methods: ['GET', 'POST'])]
    public function new(Request $request, TypeContactRepository $typeContactRepository): Response
    {
        $typeContact = new TypeContact();
        $form = $this->createForm(TypeContactType::class, $typeContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeContactRepository->save($typeContact, true);

            return $this->redirectToRoute('app_utilisateur_lib_type_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/lib/type_contact/new.html.twig', [
            'type_contact' => $typeContact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_lib_type_contact_show', methods: ['GET'])]
    public function show(TypeContact $typeContact): Response
    {
        return $this->render('utilisateur/lib/type_contact/show.html.twig', [
            'type_contact' => $typeContact,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_utilisateur_lib_type_contact_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, TypeContact $typeContact, TypeContactRepository $typeContactRepository): Response
    {
        $form = $this->createForm(TypeContactType::class, $typeContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeContactRepository->save($typeContact, true);

            return $this->redirectToRoute('app_utilisateur_lib_type_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/lib/type_contact/edit.html.twig', [
            'type_contact' => $typeContact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_lib_type_contact_delete', methods: ['POST'])]
    public function delete(Request $request, TypeContact $typeContact, TypeContactRepository $typeContactRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeContact->getId(), $request->request->get('_token'))) {
            $typeContactRepository->remove($typeContact, true);
        }

        return $this->redirectToRoute('app_utilisateur_lib_type_contact_index', [], Response::HTTP_SEE_OTHER);
    }
}
