<?php

namespace App\Controller\Utilisateur;

use App\Entity\Utilisateur\UserProfilContact;
use App\Form\Utilisateur\ContactType;
use App\Repository\Utilisateur\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/utilisateur/contact')]
class UserProfilContactController extends AbstractController
{
    #[Route('/', name: 'app_utilisateur_contact_index', methods: ['GET'])]
    public function index(ContactRepository $contactRepository): Response
    {
        return $this->render('utilisateur/contact/index.html.twig', [
            'contacts' => $contactRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_utilisateur_contact_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ContactRepository $contactRepository): Response
    {
        $contact = new UserProfilContact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactRepository->save($contact, true);

            return $this->redirectToRoute('app_utilisateur_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/contact/new.html.twig', [
            'contact' => $contact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_contact_show', methods: ['GET'])]
    public function show(UserProfilContact $contact): Response
    {
        return $this->render('utilisateur/contact/show.html.twig', [
            'contact' => $contact,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_utilisateur_contact_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserProfilContact $contact, ContactRepository $contactRepository): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactRepository->save($contact, true);

            return $this->redirectToRoute('app_utilisateur_contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utilisateur_contact_delete', methods: ['POST'])]
    public function delete(Request $request, UserProfilContact $contact, ContactRepository $contactRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $contactRepository->remove($contact, true);
        }

        return $this->redirectToRoute('app_utilisateur_contact_index', [], Response::HTTP_SEE_OTHER);
    }
}
