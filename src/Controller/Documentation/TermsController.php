<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TermsController extends AbstractController
{
    #[Route('documentation/terms', name: 'app_terms')]
    public function index(): Response
    {
        return $this->render('Documentation/terms/index.html.twig', [
            'controller_name' => 'TermsController',
        ]);
    }
}
