<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CountersController extends AbstractController
{
    #[Route('documentation/counters', name: 'app_counters')]
    public function index(): Response
    {
        return $this->render('Documentation/counters/index.html.twig', [
            'controller_name' => 'CountersController',
        ]);
    }
}
