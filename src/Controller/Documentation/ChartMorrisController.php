<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartMorrisController extends AbstractController
{
    #[Route('documentation/chart-morris', name: 'app_chart_morris')]
    public function index(): Response
    {
        return $this->render('Documentation/chart_morris/index.html.twig', [
            'controller_name' => 'ChartMorrisController',
        ]);
    }
}
