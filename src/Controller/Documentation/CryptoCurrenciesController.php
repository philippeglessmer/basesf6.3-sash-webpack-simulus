<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CryptoCurrenciesController extends AbstractController
{
    #[Route('documentation/crypto-currencies', name: 'app_crypto_currencies')]
    public function index(): Response
    {
        return $this->render('Documentation/crypto_currencies/index.html.twig', [
            'controller_name' => 'CryptoCurrenciesController',
        ]);
    }
}
