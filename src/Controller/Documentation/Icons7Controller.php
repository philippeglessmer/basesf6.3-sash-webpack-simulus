<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Icons7Controller extends AbstractController
{
    #[Route('documentation/icons7', name: 'app_icons7')]
    public function index(): Response
    {
        return $this->render('Documentation/icons7/index.html.twig', [
            'controller_name' => 'Icons7Controller',
        ]);
    }
}
