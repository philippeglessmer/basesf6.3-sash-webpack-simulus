<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ToastController extends AbstractController
{
    #[Route('documentation/toast', name: 'app_toast')]
    public function index(): Response
    {
        return $this->render('Documentation/toast/index.html.twig', [
            'controller_name' => 'ToastController',
        ]);
    }
}
