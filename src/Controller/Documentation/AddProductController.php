<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddProductController extends AbstractController
{
    #[Route('documentation/add-product', name: 'app_add_product')]
    public function index(): Response
    {
        return $this->render('Documentation/add_product/index.html.twig', [
            'controller_name' => 'AddProductController',
        ]);
    }
}
