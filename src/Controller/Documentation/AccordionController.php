<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccordionController extends AbstractController
{
    #[Route('documentation/accordion', name: 'app_accordion')]
    public function index(): Response
    {
        return $this->render('Documentation/accordion/index.html.twig', [
            'controller_name' => 'AccordionController',
        ]);
    }
}
