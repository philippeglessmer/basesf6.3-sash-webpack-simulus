<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BreadcrumbsController extends AbstractController
{
    #[Route('documentation/breadcrumbs', name: 'app_breadcrumbs')]
    public function index(): Response
    {
        return $this->render('Documentation/breadcrumbs/index.html.twig', [
            'controller_name' => 'BreadcrumbsController',
        ]);
    }
}
