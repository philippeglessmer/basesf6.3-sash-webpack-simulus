<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapsController extends AbstractController
{
    #[Route('documentation/maps', name: 'app_maps')]
    public function index(): Response
    {
        return $this->render('Documentation/maps/index.html.twig', [
            'controller_name' => 'MapsController',
        ]);
    }
}
