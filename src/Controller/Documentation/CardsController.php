<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardsController extends AbstractController
{
    #[Route('documentation/cards', name: 'app_cards')]
    public function index(): Response
    {
        return $this->render('Documentation/cards/index.html.twig', [
            'controller_name' => 'CardsController',
        ]);
    }
}
