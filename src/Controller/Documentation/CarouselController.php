<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarouselController extends AbstractController
{
    #[Route('documentation/carousel', name: 'app_carousel')]
    public function index(): Response
    {
        return $this->render('Documentation/carousel/index.html.twig', [
            'controller_name' => 'CarouselController',
        ]);
    }
}
