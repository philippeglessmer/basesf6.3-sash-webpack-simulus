<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SwitcherController extends AbstractController
{
    #[Route('documentation/switcher', name: 'app_switcher')]
    public function index(): Response
    {
        return $this->render('Documentation/switcher/index.html.twig', [
            'controller_name' => 'SwitcherController',
        ]);
    }
}
