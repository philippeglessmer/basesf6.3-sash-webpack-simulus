<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaginationController extends AbstractController
{
    #[Route('documentation/pagination', name: 'app_pagination')]
    public function index(): Response
    {
        return $this->render('Documentation/pagination/index.html.twig', [
            'controller_name' => 'PaginationController',
        ]);
    }
}
