<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/documentation', name: 'app_home_doc')]
    public function index(): Response
    {
        return $this->render('Documentation/home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
