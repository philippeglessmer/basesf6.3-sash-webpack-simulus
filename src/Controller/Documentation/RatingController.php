<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RatingController extends AbstractController
{
    #[Route('documentation/rating', name: 'app_rating')]
    public function index(): Response
    {
        return $this->render('Documentation/rating/index.html.twig', [
            'controller_name' => 'RatingController',
        ]);
    }
}
