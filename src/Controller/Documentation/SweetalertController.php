<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SweetalertController extends AbstractController
{
    #[Route('documentation/sweetalert', name: 'app_sweetalert')]
    public function index(): Response
    {
        return $this->render('Documentation/sweetalert/index.html.twig', [
            'controller_name' => 'SweetalertController',
        ]);
    }
}
