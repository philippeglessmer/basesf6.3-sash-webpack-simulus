<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TreeviewController extends AbstractController
{
    #[Route('documentation/treeview', name: 'app_treeview')]
    public function index(): Response
    {
        return $this->render('Documentation/treeview/index.html.twig', [
            'controller_name' => 'TreeviewController',
        ]);
    }
}
