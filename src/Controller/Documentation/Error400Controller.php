<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Error400Controller extends AbstractController
{
    #[Route('documentation/error400', name: 'app_error400')]
    public function index(): Response
    {
        return $this->render('Documentation/error400/index.html.twig', [
            'controller_name' => 'Error400Controller',
        ]);
    }
}
