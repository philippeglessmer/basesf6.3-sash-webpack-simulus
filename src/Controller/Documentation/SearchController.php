<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('documentation/search', name: 'app_search')]
    public function index(): Response
    {
        return $this->render('Documentation/search/index.html.twig', [
            'controller_name' => 'SearchController',
        ]);
    }
}
