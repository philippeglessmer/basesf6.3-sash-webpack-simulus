<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FileManagerController extends AbstractController
{
    #[Route('documentation/file-manager', name: 'app_file_manager')]
    public function index(): Response
    {
        return $this->render('Documentation/file_manager/index.html.twig', [
            'controller_name' => 'FileManagerController',
        ]);
    }
}
