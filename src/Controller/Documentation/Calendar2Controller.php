<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Calendar2Controller extends AbstractController
{
    #[Route('documentation/calendar2', name: 'app_calendar2')]
    public function index(): Response
    {
        return $this->render('Documentation/calendar2/index.html.twig', [
            'controller_name' => 'Calendar2Controller',
        ]);
    }
}
