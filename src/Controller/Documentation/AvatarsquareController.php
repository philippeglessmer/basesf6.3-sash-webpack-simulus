<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvatarsquareController extends AbstractController
{
    #[Route('documentation/avatarsquare', name: 'app_avatarsquare')]
    public function index(): Response
    {
        return $this->render('Documentation/avatarsquare/index.html.twig', [
            'controller_name' => 'AvatarsquareController',
        ]);
    }
}
