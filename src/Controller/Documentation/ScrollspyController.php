<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ScrollspyController extends AbstractController
{
    #[Route('documentation/scrollspy', name: 'app_scrollspy')]
    public function index(): Response
    {
        return $this->render('Documentation/scrollspy/index.html.twig', [
            'controller_name' => 'ScrollspyController',
        ]);
    }
}
