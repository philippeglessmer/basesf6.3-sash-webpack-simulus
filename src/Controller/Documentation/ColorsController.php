<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ColorsController extends AbstractController
{
    #[Route('documentation/colors', name: 'app_colors')]
    public function index(): Response
    {
        return $this->render('Documentation/colors/index.html.twig', [
            'controller_name' => 'ColorsController',
        ]);
    }
}
