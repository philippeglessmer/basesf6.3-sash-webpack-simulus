<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('documentation/profile', name: 'app_profile')]
    public function index(): Response
    {
        return $this->render('Documentation/profile/index.html.twig', [
            'controller_name' => 'ProfileController',
        ]);
    }
}
