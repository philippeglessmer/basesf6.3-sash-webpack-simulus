<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DatatableController extends AbstractController
{
    #[Route('documentation/datatable', name: 'app_datatable')]
    public function index(): Response
    {
        return $this->render('Documentation/datatable/index.html.twig', [
            'controller_name' => 'DatatableController',
        ]);
    }
}
