<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WidgetsController extends AbstractController
{
    #[Route('documentation/widgets', name: 'app_widgets')]
    public function index(): Response
    {
        return $this->render('Documentation/widgets/index.html.twig', [
            'controller_name' => 'WidgetsController',
        ]);
    }
}
