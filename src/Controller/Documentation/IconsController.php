<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IconsController extends AbstractController
{
    #[Route('documentation/icons', name: 'app_icons')]
    public function index(): Response
    {
        return $this->render('Documentation/icons/index.html.twig', [
            'controller_name' => 'IconsController',
        ]);
    }
}
