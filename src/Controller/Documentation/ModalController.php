<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ModalController extends AbstractController
{
    #[Route('documentation/modal', name: 'app_modal')]
    public function index(): Response
    {
        return $this->render('Documentation/modal/index.html.twig', [
            'controller_name' => 'ModalController',
        ]);
    }
}
