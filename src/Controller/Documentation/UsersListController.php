<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsersListController extends AbstractController
{
    #[Route('documentation/users-list', name: 'app_users_list')]
    public function index(): Response
    {
        return $this->render('Documentation/users_list/index.html.twig', [
            'controller_name' => 'UsersListController',
        ]);
    }
}
