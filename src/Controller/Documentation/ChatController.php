<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    #[Route('documentation/chat', name: 'app_chat')]
    public function index(): Response
    {
        return $this->render('Documentation/chat/index.html.twig', [
            'controller_name' => 'ChatController',
        ]);
    }
}
