<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TypographyController extends AbstractController
{
    #[Route('documentation/typography', name: 'app_typography')]
    public function index(): Response
    {
        return $this->render('Documentation/typography/index.html.twig', [
            'controller_name' => 'TypographyController',
        ]);
    }
}
