<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogDetailsController extends AbstractController
{
    #[Route('documentation/blog-details', name: 'app_blog_details')]
    public function index(): Response
    {
        return $this->render('Documentation/blog_details/index.html.twig', [
            'controller_name' => 'BlogDetailsController',
        ]);
    }
}
