<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendarController extends AbstractController
{
    #[Route('documentation/calendar', name: 'app_calendar')]
    public function index(): Response
    {
        return $this->render('Documentation/calendar/index.html.twig', [
            'controller_name' => 'CalendarController',
        ]);
    }
}
