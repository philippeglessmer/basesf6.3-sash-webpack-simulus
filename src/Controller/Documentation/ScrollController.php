<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ScrollController extends AbstractController
{
    #[Route('documentation/scroll', name: 'app_scroll')]
    public function index(): Response
    {
        return $this->render('Documentation/scroll/index.html.twig', [
            'controller_name' => 'ScrollController',
        ]);
    }
}
