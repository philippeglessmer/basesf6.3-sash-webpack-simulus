<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvatarRoundController extends AbstractController
{
    #[Route('documentation/avatar-round', name: 'app_avatar_round')]
    public function index(): Response
    {
        return $this->render('Documentation/avatar_round/index.html.twig', [
            'controller_name' => 'AvatarRoundController',
        ]);
    }
}
