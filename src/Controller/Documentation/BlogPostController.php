<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogPostController extends AbstractController
{
    #[Route('documentation/blog-post', name: 'app_blog_post')]
    public function index(): Response
    {
        return $this->render('Documentation/blog_post/index.html.twig', [
            'controller_name' => 'BlogPostController',
        ]);
    }
}
