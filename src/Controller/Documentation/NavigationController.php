<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavigationController extends AbstractController
{
    #[Route('documentation/navigation', name: 'app_navigation')]
    public function index(): Response
    {
        return $this->render('Documentation/navigation/index.html.twig', [
            'controller_name' => 'NavigationController',
        ]);
    }
}
