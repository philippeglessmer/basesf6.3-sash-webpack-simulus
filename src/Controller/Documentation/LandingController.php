<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    #[Route('documentation/landing', name: 'app_landing')]
    public function index(): Response
    {
        return $this->render('Documentation/landing/index.html.twig', [
            'controller_name' => 'LandingController',
        ]);
    }
}
