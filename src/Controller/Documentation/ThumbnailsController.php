<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ThumbnailsController extends AbstractController
{
    #[Route('documentation/thumbnails', name: 'app_thumbnails')]
    public function index(): Response
    {
        return $this->render('Documentation/thumbnails/index.html.twig', [
            'controller_name' => 'ThumbnailsController',
        ]);
    }
}
