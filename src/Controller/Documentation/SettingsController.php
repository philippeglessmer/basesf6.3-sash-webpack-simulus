<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SettingsController extends AbstractController
{
    #[Route('documentation/settings', name: 'app_settings')]
    public function index(): Response
    {
        return $this->render('Documentation/settings/index.html.twig', [
            'controller_name' => 'SettingsController',
        ]);
    }
}
