<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    #[Route('documentation/shop', name: 'app_shop')]
    public function index(): Response
    {
        return $this->render('Documentation/shop/index.html.twig', [
            'controller_name' => 'ShopController',
        ]);
    }
}
