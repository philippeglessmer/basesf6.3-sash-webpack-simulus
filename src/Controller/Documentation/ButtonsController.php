<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ButtonsController extends AbstractController
{
    #[Route('documentation/buttons', name: 'app_buttons')]
    public function index(): Response
    {
        return $this->render('Documentation/buttons/index.html.twig', [
            'controller_name' => 'ButtonsController',
        ]);
    }
}
