<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FootersController extends AbstractController
{
    #[Route('documentation/footers', name: 'app_footers')]
    public function index(): Response
    {
        return $this->render('Documentation/footers/index.html.twig', [
            'controller_name' => 'FootersController',
        ]);
    }
}
