<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    #[Route('documentation/blog', name: 'app_blog')]
    public function index(): Response
    {
        return $this->render('Documentation/blog/index.html.twig', [
            'controller_name' => 'BlogController',
        ]);
    }
}
