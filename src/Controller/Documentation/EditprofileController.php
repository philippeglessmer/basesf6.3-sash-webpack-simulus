<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditprofileController extends AbstractController
{
    #[Route('documentation/editprofile', name: 'app_editprofile')]
    public function index(): Response
    {
        return $this->render('Documentation/editprofile/index.html.twig', [
            'controller_name' => 'EditprofileController',
        ]);
    }
}
