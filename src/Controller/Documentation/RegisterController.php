<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    #[Route('documentation/register', name: 'app_register1')]
    public function index(): Response
    {
        return $this->render('Documentation/register/index.html.twig', [
            'controller_name' => 'RegisterController',
        ]);
    }
}
