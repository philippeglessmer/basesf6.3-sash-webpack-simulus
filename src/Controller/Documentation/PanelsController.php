<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PanelsController extends AbstractController
{
    #[Route('documentation/panels', name: 'app_panels')]
    public function index(): Response
    {
        return $this->render('Documentation/panels/index.html.twig', [
            'controller_name' => 'PanelsController',
        ]);
    }
}
