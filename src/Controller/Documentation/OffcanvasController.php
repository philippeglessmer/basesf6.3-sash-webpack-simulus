<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OffcanvasController extends AbstractController
{
    #[Route('documentation/offcanvas', name: 'app_offcanvas')]
    public function index(): Response
    {
        return $this->render('Documentation/offcanvas/index.html.twig', [
            'controller_name' => 'OffcanvasController',
        ]);
    }
}
