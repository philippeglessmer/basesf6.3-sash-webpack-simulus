<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RangesliderController extends AbstractController
{
    #[Route('documentation/rangeslider', name: 'app_rangeslider')]
    public function index(): Response
    {
        return $this->render('Documentation/rangeslider/index.html.twig', [
            'controller_name' => 'RangesliderController',
        ]);
    }
}
