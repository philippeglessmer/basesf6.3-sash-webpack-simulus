<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Error503Controller extends AbstractController
{
    #[Route('documentation/error503', name: 'app_error503')]
    public function index(): Response
    {
        return $this->render('Documentation/error503/index.html.twig', [
            'controller_name' => 'Error503Controller',
        ]);
    }
}
