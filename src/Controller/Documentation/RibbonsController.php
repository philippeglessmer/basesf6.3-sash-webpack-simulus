<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RibbonsController extends AbstractController
{
    #[Route('documentation/ribbons', name: 'app_ribbons')]
    public function index(): Response
    {
        return $this->render('Documentation/ribbons/index.html.twig', [
            'controller_name' => 'RibbonsController',
        ]);
    }
}
