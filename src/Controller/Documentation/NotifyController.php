<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotifyController extends AbstractController
{
    #[Route('documentation/notify', name: 'app_notify')]
    public function index(): Response
    {
        return $this->render('Documentation/notify/index.html.twig', [
            'controller_name' => 'NotifyController',
        ]);
    }
}
