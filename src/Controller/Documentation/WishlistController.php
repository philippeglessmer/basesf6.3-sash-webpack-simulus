<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WishlistController extends AbstractController
{
    #[Route('documentation/wishlist', name: 'app_wishlist')]
    public function index(): Response
    {
        return $this->render('Documentation/wishlist/index.html.twig', [
            'controller_name' => 'WishlistController',
        ]);
    }
}
