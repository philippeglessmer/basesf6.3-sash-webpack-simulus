<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('documentation/cart', name: 'app_cart')]
    public function index(): Response
    {
        return $this->render('Documentation/cart/index.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }
}
