<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TagsController extends AbstractController
{
    #[Route('documentation/tags', name: 'app_tags')]
    public function index(): Response
    {
        return $this->render('Documentation/tags/index.html.twig', [
            'controller_name' => 'TagsController',
        ]);
    }
}
