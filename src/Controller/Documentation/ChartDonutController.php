<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartDonutController extends AbstractController
{
    #[Route('documentation/chart-donut', name: 'app_chart_donut')]
    public function index(): Response
    {
        return $this->render('Documentation/chart_donut/index.html.twig', [
            'controller_name' => 'ChartDonutController',
        ]);
    }
}
