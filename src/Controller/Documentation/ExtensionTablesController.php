<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExtensionTablesController extends AbstractController
{
    #[Route('documentation/extensio-tables', name: 'app_extension_tables')]
    public function index(): Response
    {
        return $this->render('Documentation/extension_tables/index.html.twig', [
            'controller_name' => 'ExtensionTablesController',
        ]);
    }
}
