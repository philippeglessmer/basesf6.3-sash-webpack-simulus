<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProgressController extends AbstractController
{
    #[Route('documentation/progress', name: 'app_progress')]
    public function index(): Response
    {
        return $this->render('Documentation/progress/index.html.twig', [
            'controller_name' => 'ProgressController',
        ]);
    }
}
