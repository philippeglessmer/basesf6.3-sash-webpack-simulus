<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    #[Route('documentation/invoice', name: 'app_invoice')]
    public function index(): Response
    {
        return $this->render('Documentation/invoice/index.html.twig', [
            'controller_name' => 'InvoiceController',
        ]);
    }
}
