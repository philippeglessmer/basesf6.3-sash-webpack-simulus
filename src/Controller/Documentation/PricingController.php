<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PricingController extends AbstractController
{
    #[Route('documentation/pricing', name: 'app_pricing')]
    public function index(): Response
    {
        return $this->render('Documentation/pricing/index.html.twig', [
            'controller_name' => 'PricingController',
        ]);
    }
}
