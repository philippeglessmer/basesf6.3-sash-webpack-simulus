<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartController extends AbstractController
{
    #[Route('documentation/chart', name: 'app_chart')]
    public function index(): Response
    {
        return $this->render('Documentation/chart/index.html.twig', [
            'controller_name' => 'ChartController',
        ]);
    }
}
