<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartFlotController extends AbstractController
{
    #[Route('documentation/chart-folt', name: 'app_chart_flot')]
    public function index(): Response
    {
        return $this->render('Documentation/chart_flot/index.html.twig', [
            'controller_name' => 'ChartFlotController',
        ]);
    }
}
