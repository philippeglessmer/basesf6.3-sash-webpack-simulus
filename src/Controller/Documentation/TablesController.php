<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TablesController extends AbstractController
{
    #[Route('documentation/tables', name: 'app_tables')]
    public function index(): Response
    {
        return $this->render('Documentation/tables/index.html.twig', [
            'controller_name' => 'TablesController',
        ]);
    }
}
