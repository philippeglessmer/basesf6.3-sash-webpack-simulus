<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServicesController extends AbstractController
{
    #[Route('documentation/services', name: 'app_services')]
    public function index(): Response
    {
        return $this->render('Documentation/services/index.html.twig', [
            'controller_name' => 'ServicesController',
        ]);
    }
}
