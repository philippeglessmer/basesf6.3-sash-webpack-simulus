<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlertsController extends AbstractController
{
    #[Route('documentation/alerts', name: 'app_alerts')]
    public function index(): Response
    {
        return $this->render('Documentation/alerts/index.html.twig', [
            'controller_name' => 'AlertsController',
        ]);
    }
}
