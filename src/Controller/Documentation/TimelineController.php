<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TimelineController extends AbstractController
{
    #[Route('documentation/timeline', name: 'app_timeline')]
    public function index(): Response
    {
        return $this->render('Documentation/timeline/index.html.twig', [
            'controller_name' => 'TimelineController',
        ]);
    }
}
