<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Error403Controller extends AbstractController
{
    #[Route('documentation/error403', name: 'app_error403')]
    public function index(): Response
    {
        return $this->render('Documentation/error403/index.html.twig', [
            'controller_name' => 'Error403Controller',
        ]);
    }
}
