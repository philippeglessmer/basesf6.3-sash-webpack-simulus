<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoadersController extends AbstractController
{
    #[Route('documentation/loaders', name: 'app_loaders')]
    public function index(): Response
    {
        return $this->render('Documentation/loaders/index.html.twig', [
            'controller_name' => 'LoadersController',
        ]);
    }
}
