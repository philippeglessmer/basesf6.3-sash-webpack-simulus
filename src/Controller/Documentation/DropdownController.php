<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DropdownController extends AbstractController
{
    #[Route('documentation/dropdown', name: 'app_dropdown')]
    public function index(): Response
    {
        return $this->render('Documentation/dropdown/index.html.twig', [
            'controller_name' => 'DropdownController',
        ]);
    }
}
