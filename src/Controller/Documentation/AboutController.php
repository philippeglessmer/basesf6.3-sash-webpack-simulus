<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    #[Route('documentation/about', name: 'app_about')]
    public function index(): Response
    {
        return $this->render('Documentation/about/index.html.twig', [
            'controller_name' => 'AboutController',
        ]);
    }
}
