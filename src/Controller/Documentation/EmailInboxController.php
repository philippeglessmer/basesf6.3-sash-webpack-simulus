<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailInboxController extends AbstractController
{
    #[Route('documentation/email-inbox', name: 'app_email_inbox')]
    public function index(): Response
    {
        return $this->render('Documentation/email_inbox/index.html.twig', [
            'controller_name' => 'EmailInboxController',
        ]);
    }
}
