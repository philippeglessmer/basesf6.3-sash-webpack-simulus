<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Maps1Controller extends AbstractController
{
    #[Route('documentation/maps1', name: 'app_maps1')]
    public function index(): Response
    {
        return $this->render('Documentation/maps1/index.html.twig', [
            'controller_name' => 'Maps1Controller',
        ]);
    }
}
