<?php

namespace App\Controller\Documentation;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BadgeController extends AbstractController
{
    #[Route('documentation/badge', name: 'app_badge')]
    public function index(): Response
    {
        return $this->render('Documentation/badge/index.html.twig', [
            'controller_name' => 'BadgeController',
        ]);
    }
}
