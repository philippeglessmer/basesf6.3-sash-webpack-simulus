<?php

namespace App\Entity\Utilisateur\Lib;

use App\Entity\Utilisateur\UserProfilContact;
use App\Repository\Utilisateur\Lib\TypeContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypeContactRepository::class)]
class TypeContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $libelle = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $icon = null;

    #[ORM\OneToMany(mappedBy: 'typeContact', targetEntity: UserProfilContact::class)]
    private Collection $userProfilContacts;

    public function __construct()
    {
        $this->userProfilContacts = new ArrayCollection();
    }


    public function __toString(): string
    {
        return $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection<int, UserProfilContact>
     */
    public function getUserProfilContacts(): Collection
    {
        return $this->userProfilContacts;
    }

    public function addUserProfilContact(UserProfilContact $userProfilContact): self
    {
        if (!$this->userProfilContacts->contains($userProfilContact)) {
            $this->userProfilContacts->add($userProfilContact);
            $userProfilContact->setTypeContact($this);
        }

        return $this;
    }

    public function removeUserProfilContact(UserProfilContact $userProfilContact): self
    {
        if ($this->userProfilContacts->removeElement($userProfilContact)) {
            // set the owning side to null (unless already changed)
            if ($userProfilContact->getTypeContact() === $this) {
                $userProfilContact->setTypeContact(null);
            }
        }

        return $this;
    }
}
