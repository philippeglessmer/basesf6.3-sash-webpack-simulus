<?php

namespace App\Entity\Utilisateur;


use App\Entity\Utilisateur\Lib\TypeContact;
use App\Repository\Utilisateur\UserProfilContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserProfilContactRepository::class)]
class UserProfilContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 200, nullable: true)]
    private ?string $content = null;

    #[ORM\ManyToOne(inversedBy: 'contacts')]
    private ?UserProfil $profilUser = null;

    #[ORM\ManyToOne(inversedBy: 'userProfilContacts')]
    private ?TypeContact $typeContact = null;

    public function __construct()
    {
        $this->type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getProfilUser(): ?UserProfil
    {
        return $this->profilUser;
    }

    public function setProfilUser(?UserProfil $profilUser): self
    {
        $this->profilUser = $profilUser;

        return $this;
    }

    public function getTypeContact(): ?TypeContact
    {
        return $this->typeContact;
    }

    public function setTypeContact(?TypeContact $typeContact): self
    {
        $this->typeContact = $typeContact;

        return $this;
    }
}
