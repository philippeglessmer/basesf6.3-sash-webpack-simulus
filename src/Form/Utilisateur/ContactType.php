<?php

namespace App\Form\Utilisateur;

use App\Entity\Utilisateur\Lib\TypeContact;
use App\Entity\Utilisateur\UserProfilContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('typeContact', null, [
                'label' => 'Type de contact',
                'placeholder' => ' -- choisissez --',
            ])
            ->add('content', TextType::class, [
                'label' => 'Numéro, URL, ...'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserProfilContact::class,
        ]);
    }
}
