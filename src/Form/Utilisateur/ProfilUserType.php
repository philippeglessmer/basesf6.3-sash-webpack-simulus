<?php

namespace App\Form\Utilisateur;

use App\Entity\Utilisateur\UserProfil;
use App\Form\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use function Symfony\Component\Translation\t;

class ProfilUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('genre', ChoiceType::class, [
                'choices' => ['Monsieur' => 1, 'Madame' => 2],
                'placeholder' => '-- Choisissez',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('nom', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('prenom', TextType::class, [
                'label'=> 'Prénom',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('naissanceAt', BirthdayType::class, [
                'label'=> 'Date de naissance',
                'required' => false,
                'attr' => [

                ]
            ])
            ->add('biographie', TextareaType::class, [
                'label'=> 'A mon sujet',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('file', FileType::class, [
                'label'=> 'Photo de profil',
                'mapped' => false,
                'attr' => [

                ]
            ])
            ->add('email', EmailType::class, [
                'mapped' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'mapped' => false,
                'choices' => ['Administrateur ' => 'ROLE_ADMIN', 'Utilisateur' => 'ROLE_USER'],
                'placeholder' => '-- Choisissez',
                'attr' => [
                    'class' => 'form-control',
                ],
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('contacts', CollectionType::class, [
                'required' => false,
                'label' => false,
                'entry_type' => ContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'label'=> 'Mot de passe',
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserProfil::class,
        ]);
    }
}
